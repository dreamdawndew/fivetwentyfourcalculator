import sys
import pandas as pd
import time
import datetime
from datetime import date, datetime, timedelta
from PyQt5 import QtWidgets
from PyQt5 import uic
from PyQt5.QtWidgets import QApplication, QWidget, QPushButton, QMessageBox, QFileDialog, QLineEdit
 
class Form(QtWidgets.QDialog):
    #Initialize and Show UI
    def __init__(self, parent=None):
        QtWidgets.QDialog.__init__(self, parent)
        self.ui = uic.loadUi("fTFreal.ui")
        self.ui.show()
        self.ui.pushButton.clicked.connect(self.pushButton_click)

    #Basically everything done here
    def pushButton_click(self):
        fname = QFileDialog.getOpenFileName(self, 'Open file', 'c:\\',"Excel files (*.xlsm *.xlsx *.xls)")
        df = pd.read_excel(fname[0])
        count524 = 0
        isOver524 = False
        list524 = []

        #For every cards, calculate day difference between today and card open date
        for dates in df["Open Date"]:
            if (datetime.now().date() - dates.date()).days < 730: #if less than 730 days have passed since opening the card
                count524 += 1 #increment 524 cards count
                list524.append(dates + timedelta(days=731)) #add to the 524 date list
                
        list524 = sorted(list524, reverse=True) #Sort dates by descending order
        
        if (len(list524)>=5): #If the number of the cards opened within last 24 months is larger than or equal to 5
            while (len(list524) > 4):
                final524 = list524.pop() #Discard the cards from the list from the newest one until the number of the cards reaches 4

            resultBox = QMessageBox()
            resultBox.setWindowTitle("5/24!")
            resultBox.setStyleSheet('QMessageBox {text-align: center;}') 
            resultBox.setText("You are currently at " + str(count524) + #Printing results if 524
                          "/24.\n\nYou will reach 4/24 on " + final524.strftime("%Y-%m-%d") +
                            ".\n\nYou will reach 3/24 on " + list524[-1].strftime("%Y-%m-%d") +
                              ".\n\nYou will reach 2/24 on " + list524[-2].strftime("%Y-%m-%d") +
                              ".\n\nYou will reach 1/24 on " + list524[-3].strftime("%Y-%m-%d") +
                              ".\n\nYou will reach 0/24 on " + list524[0].strftime("%Y-%m-%d") + ".")

            resultBox.exec_()
        else:
            resultBox = QMessageBox()
            resultBox.setWindowTitle("No 5/24") #Printing results if not 524
            resultBox.setStyleSheet('QMessageBox {text-align: center;}')
            resultBox.setText("You are under 5/24. You have only " + str(len(list524)) + " card(s) opened in the past 24 months.")
            resultBox.exec_()

if __name__ == '__main__':
    app = QtWidgets.QApplication(sys.argv)
    w = Form()
    sys.exit(app.exec())
